<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	protected $_PDATA;

	function __construct()
	{
		parent::__construct();
		$this->_PDATA = $this->input->post(NULL, TRUE );
	}

	public function check()
	{
		$this->load->model('Adop/auth_model');
		$_return = $this->auth_model->session_check();

	}

	public function ssession_check()
	{
		$_RETURN['status'] = session();
		$this->output->set_header('Access-Control-Allow-Origin: *')->
		set_content_type('application/json')->
		set_output(json_encode($_RETURN));
	}

	## 로그인
	public function login()
	{
		$this->view->view_page();
	}

	## 회원가입
	public function insert()
	{
		$this->load->model('Adop/auth_model');
		$this->auth_model->login_insert();
	}
}
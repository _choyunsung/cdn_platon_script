<?php

function GUUID()
{
	if (function_exists('com_create_guid') === true)
	{
		return trim(com_create_guid(), '{}');
	}

	return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function session()
{
	if(isset($_SESSION['PLATON']['ADMIN']))
	{
		return true;
	}else{
		return false;
	}

}

function Redirects($url, $code=302)
{
	header('Location: '.$url, TRUE, $code);
	exit;
}
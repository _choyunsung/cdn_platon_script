<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * 참조 사이트 :
 * https://github.com/phpredis/phpredis#flushdb
 * 
 * */

class Elasticache
{
    private $_CI;
    private $_connection;
    private $_configs;
    
    
    
    public function __construct()
    {
        $this->_CI = &get_instance();
        
        $this->_redis = new Redis();
        $this->_CI->load->config('redis');
        $this->_configs = $this->_CI->config->item('redis');
        
    }
    
    
    public function &connection($_connect_check='master')
    {
        if(is_array($this->_configs))
        {
            if($_connect_check=='master')
            {
                $this->_connection = $this->_redis->{$this->_configs['master']['type']}($this->_configs['master']['hostname'],$this->_configs['master']['port']);
                $this->_redis->auth($this->_configs['master']['password']);
                
            }else{
                $_slave_count = mt_rand(0,count($this->_configs['slave'])-1);
                $_slave_conn = $this->_configs['slave'][$_slave_count];
                $this->_connection = $this->_redis->{$_slave_conn['type']}($_slave_conn['hostname'],$_slave_conn['port']);
                $this->_redis->auth($_slave_conn['password']);
                
            }

            $this->_redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
            
            if(empty($this->_configs['master']['prefix']))
                $this->_redis->setOption(Redis::OPT_PREFIX, $this->_configs['master']['prefix'] );
            return $this->_redis;
            
        }else{
            show_error('Could not connect to Redis');
        }
        
    }
    
    public function tPut($keys, $value, $date = NULL)
    {
        $this->connection();
        $this->_redis->setTimeout($keys, $value);

    }
    
    ### 타임 
    public function fPut($keys, $value)
    {
        $this->connection();
        $this->_redis->setTimeout($keys, $value);
        
    }
   
    ### 기존의 키에 데이터 추가 
    public function append($keys, $value )
    {
        $this->connection();
        $this->_redis->append($keys, $value );
        
        
    }
    
    public function put( $keys, $value, $optins = '' )
    {
        $this->connection();
        if(is_array($optins))
            $this->_redis->set($keys, $value, $optins);
        else
        {
            if($optins) {
                $this->_redis->delete($keys);
                $this->_redis->hSet($optins, $keys, $value);
            }else {
                $this->_redis->set($keys, $value);
            }
                
        }
            
        
        
    }
    
    public function getAll($keys)
    {
        $this->connection('slave');
        return $this->_redis->hVals($keys);        
    }
    
    public function get( $keys , $hash = false )
    {
        $this->connection('slave');
        if($hash)
        {
            return $this->_redis->hGet($hash, $keys);
        }else{
            return $this->_redis->get($keys);
        }
        
        
    }
    
    public function delete($key)
    {
        
        
    }
    
    public function flashDB()
    {
        
    }
    
    public function rows_num()
    {
        $this->connection();
        return $this->_redis->dbSize();
    }
    
    public function status($option='')
    {
        /*
        --------------- $options type -------
        COMMANDSTATS - Information on the commands
        CPU - CPU information from Redis
        */
        return $this->_redis->info($option);
    }
    
    
    function __destrcut()
    {
        if($this->_configs['master']['type']=='pconnect')
            if ($this->_connection) $this->_redis->close();

        $this->_redis->close();
        
    }
    
}
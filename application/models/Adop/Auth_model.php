<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	protected $_PDATA;
	private $_MDB;
	private $_SDB;
	private $_CACHE;
	private $_REDIS;

	public function __construct()
	{
		parent::__construct();

		$this->_PDATA = $this->input->post(NULL,TRUE);
	}

	/*
	  CREATE TABLE `members` (
	  `guuid` varchar(37) NOT NULL DEFAULT '' COMMENT '고유 GUUID',
	  `id` varchar(70) NOT NULL DEFAULT '' COMMENT '아이디',
	  `password` varchar(128) DEFAULT NULL COMMENT '암호',
	  `se_key` varchar(128) NOT NULL DEFAULT '' COMMENT '시크릿키',
	  `compay_key` varchar(128) DEFAULT NULL,
	  `status` enum('Y','N','V','D') NOT NULL DEFAULT 'V' COMMENT '상태',
	  `level` int(11) DEFAULT NULL COMMENT '레벨 권한',
	  PRIMARY KEY (`guuid`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	*/

	public function session_check()
	{
		session_start();
		if(isset($_SESSION['PLATON']['ADMIN']))
		{
			if($this->session_set())
			{

			}

		}else{
			Redirects('/auth/login');
		}
	}

	### 로그인 체크
	public function login_check()
	{
		$this->_MDB = $this->load->database('platon_master', TRUE);

		$_query_params = array($this->_PDATA['id']);
		if(!$this->_PDATA['id'])
			show_error('id empty');

		$this->_MDB->cache_on();
		$_query = $this->_MDB->query("SELECT * FROM members WHERE id = ? ", $_query_params );
		$_rows = $_query->result_array();
		var_dump($_rows);


	}

	### 로그인 저장
	public function login_insert()
	{

		$this->_MDB = $this->load->database('platon_master', TRUE);

		if(empty($this->_PDATA['id']))
			show_error('id empty');

		$_query_params = array(
			$this->_PDATA['id']
		);

		$_query = $this->_MDB->query("SELECT * FROM members WHERE id = ? ", $_query_params );

		if($_query->num_rows() > 0 )
		{
			$_RETURN = array(
				'request' => (int)200,
				'code' => 'E01',
				'msg' => 'can not using id'
			);

			$this->output->set_status_header(210)->
				set_content_type('application/json')->
				set_output(json_encode($_RETURN));

		}else{

			if(empty($this->_PDATA['passwd']) || empty($this->_PDATA['se_pass']) )
				show_error('password or se_pass is empty');

			$_insert_params = array(
				GUUID(),
				$this->_PDATA['id'],
				$this->_PDATA['passwd'],
				$this->_PDATA['se_pass']
			);

			$_insert_query = $this->_MDB->query("INSERT INTO members (key, id, password, se_key ) values (?, ?, password(?), password(?) )",$_insert_params);


		}

	}

	public function session_set()
	{
		session_start();


	}

	### 사용자 레벨 셋팅
	public function level_setting()
	{



	}

	### 부가 정보 입력
	public function company_insert()
	{



	}



}
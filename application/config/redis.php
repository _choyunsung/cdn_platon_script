<?php
/*
===============================================================================
2015.12.16 - steven(steven@adop.co.kr)
REDIS CACHE CONFIG [ MASTER vs SLAVE ] 

===============================================================================
*/

$config['redis']['master'] = array( 
    'hostname' => 'optimaaredis.vfswfn.0001.apne1.cache.amazonaws.com' ,
    'port' => 6379 ,
    'password' => '',
    'type' => 'connect',
    'delay' => 0.5,
    'reconnect' => TRUE,
    'recon_delay' => 0.5,
    'prefix' => 'platon:'
);

$config['redis']['slave'] = array(
    array( 
        'hostname' => 'optimaaredis.vfswfn.0001.apne1.cache.amazonaws.com' ,
        'port' => '6379' ,
        'password' => '' ,
        'type' => 'connect',
        'delay' => 0.5,
        'reconnect' => TRUE,
        'recon_delay' => 0.5
    )
);